NAME = libftprintf.a
SRC = ft_printf.c func_letter.c struct_making.c s_func.c\
	  d_func.c u_func.c x_func.c c_func.c c_big_func.c\
	  padding_min.c padding_plus.c s_big_func.c
OBJ = $(SRC:.c=.o)
HEAD = ft_printf.h
FLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ)
	make -C libft/
	ar -cru $(NAME) $(OBJ) libft/*.o
	@echo libftprintf.a has been made

%.o: %.c
		gcc $(FLAGS) -c -o $@ $<

cleanlib:
	make clean -C ./libft

fcleanlib:
	make fclean -C ./libft

clean: cleanlib
	rm -f $(OBJ)
	@echo *.o have been cleaned.

fclean: clean fcleanlib
	rm -f $(NAME)
	@echo *.a and *.o have been cleaned.

re: fclean all
	
