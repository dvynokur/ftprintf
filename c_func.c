/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_func.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvynokur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/23 13:30:50 by dvynokur          #+#    #+#             */
/*   Updated: 2017/03/23 13:30:52 by dvynokur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		func_c(t_p_list *data, va_list *ap)
{
	unsigned int	c;

	if (data->let == '%')
		return (padding_char(data, '%'));
	if (data->let == 'c' || data->let == 'C')
	{
		c = va_arg(*ap, int);
		return (padding_char(data, (char)c));
	}
	if (data->let != '%' && data->let != 'c'
		&& data->let != 'C' && data->let != 'n')
		return (padding_char(data, data->let));
	return (0);
}

int		padding_char(t_p_list *data, char a)
{
	char	c;
	int		i;

	i = 0;
	if (c_find(data->flags, '0') == 1)
		c = '0';
	else
		c = ' ';
	if (data->width >= 1)
		return (char_w_higher(data, a, c));
	else
	{
		ft_putchar(a);
		return (1);
	}
	return (0);
}

int		char_w_higher(t_p_list *data, char a, char c)
{
	int i;

	i = 0;
	if (c_find(data->flags, '-') == 1)
	{
		ft_putchar(a);
		while (i < (data->width - 1))
		{
			ft_putchar(' ');
			i++;
		}
		return (data->width);
	}
	else
	{
		while (i < (data->width - 1))
		{
			ft_putchar(c);
			i++;
		}
		ft_putchar(a);
		return (data->width);
	}
}
