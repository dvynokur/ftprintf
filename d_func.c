/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   d_func.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvynokur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 19:20:05 by dvynokur          #+#    #+#             */
/*   Updated: 2017/03/03 19:20:08 by dvynokur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			sign_check(char *flags, int min)
{
	if (min == -1)
		return (0);
	if (min == 1)
		return (1);
	if (c_find(flags, '+') == 1)
		return (1);
	if (c_find(flags, ' ') == 1)
		return (1);
	return (0);
}

int			sign_printing(char *flags, int min)
{
	int n;

	n = 0;
	if (min == -1)
		return (0);
	if (min == 1)
	{
		write(1, "-", 1);
		return (1);
	}
	if (c_find(flags, '+') == 1)
	{
		write(1, "+", 1);
		return (1);
	}
	if (c_find(flags, ' ') == 1)
	{
		write(1, " ", 1);
		return (1);
	}
	return (0);
}

intmax_t	size_mod(t_p_list *data, va_list *ap)
{
	intmax_t	d;
	char		*s;
	char		c;

	d = 0;
	s = data->size_mod;
	c = data->let;
	if ((ft_strcmp(s, "l") == 0) || (c == 'D'))
	{
		d = (long)va_arg(*ap, intmax_t);
		return (d);
	}
	if (((ft_strcmp(s, "") == 0)) && (d = (int)va_arg(*ap, intmax_t)))
		return (d);
	if (((ft_strcmp(s, "hh") == 0)) && (d = (char)va_arg(*ap, intmax_t)))
		return (d);
	if (((ft_strcmp(s, "h") == 0)) && (d = (short)va_arg(*ap, intmax_t)))
		return (d);
	if ((ft_strcmp(s, "ll") == 0) && (d = (long long)va_arg(*ap, intmax_t)))
		return (d);
	if ((ft_strcmp(s, "j") == 0) && (d = (intmax_t)va_arg(*ap, intmax_t)))
		return (d);
	if ((ft_strcmp(s, "z") == 0) && (d = (size_t)va_arg(*ap, intmax_t)))
		return (d);
	return (0);
}

int			func_d(t_p_list *data, va_list *ap)
{
	intmax_t	d;
	int			len;
	int			ret;
	char		*num;

	ret = 0;
	d = size_mod(data, ap);
	if (d < 0)
		data->min = 1;
	num = ft_itoa_max(d);
	len = ft_strlen(num);
	if (c_find(data->flags, '-') == 1)
		return (padding_d_min(data, len, num));
	else
		return (padding_d(data, len, num));
	return (0);
}

char		*ft_itoa_max(intmax_t n)
{
	char		*res;
	size_t		len;
	uintmax_t	b;

	if (n < 0)
		b = n * (-1);
	else
		b = n;
	len = len_calc(b, 10);
	res = (char*)malloc(sizeof(char) * len + 1);
	if (!res)
		return (NULL);
	res[len] = '\0';
	while (len--)
	{
		res[len] = b % 10 + '0';
		b = b / 10;
	}
	return (res);
}
