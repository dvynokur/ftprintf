/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   C_func.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvynokur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/25 19:17:06 by dvynokur          #+#    #+#             */
/*   Updated: 2017/03/25 19:17:09 by dvynokur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		shifting_bytes(unsigned int c)
{
	unsigned int	mask;
	int				num;

	num = char_len(c);
	if (num == 1)
	{
		ft_putchar((char)c);
		return (1);
	}
	if (num == 2)
	{
		mask = 49280;
		ft_putchar((mask >> 8) | (c >> 6));
		ft_putchar((mask & ~65280) | (c & ~1984));
		return (2);
	}
	else
		return (shifting_bytes_big(c));
	return (0);
}

int		shifting_bytes_big(unsigned int c)
{
	unsigned int	mask;
	int				num;

	num = char_len(c);
	if (num == 3)
	{
		mask = 14712960;
		ft_putchar((mask >> 16) | (c >> 12));
		ft_putchar((mask >> 8 & ~65280) | (c >> 6 & ~65472));
		ft_putchar((mask & ~16711680) | (c & ~65472));
		return (3);
	}
	else if (num == 4)
	{
		mask = 4034953344;
		ft_putchar((mask >> 24) | (c >> 18));
		ft_putchar((mask >> 16 & ~65280) | (c >> 12 & ~2097088));
		ft_putchar((mask >> 8 & ~16711680) | (c >> 6 & ~2097088));
		ft_putchar(mask & ~4278190080);
		return (4);
	}
	return (0);
}

int		width_higher_one(t_p_list *data, unsigned int a, int n, char c)
{
	int i;

	i = 0;
	if (c_find(data->flags, '-') == 1)
	{
		shifting_bytes(a);
		while (i < (data->width - n))
		{
			ft_putchar(' ');
			i++;
		}
		return (data->width);
	}
	else
	{
		while (i < (data->width - n))
		{
			ft_putchar(c);
			i++;
		}
		shifting_bytes(a);
		return (data->width);
	}
}

int		padding_wchar(t_p_list *data, unsigned int a, int n)
{
	char	c;
	int		i;

	i = 0;
	if (c_find(data->flags, '0') == 1)
		c = '0';
	else
		c = ' ';
	if ((data->width - n) >= 1)
		return (width_higher_one(data, a, n, c));
	else
	{
		shifting_bytes(a);
		return (n);
	}
	return (0);
}
