/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   padding_plus.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvynokur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/08 19:33:43 by dvynokur          #+#    #+#             */
/*   Updated: 2017/04/08 19:33:45 by dvynokur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		len_higher_prec_plus(t_p_list *data, char *d, char *s, int len)
{
	int l;

	l = 0;
	if (data->let != 'p')
		l = ft_strlen(s);
	if (data->prec == 0 && d[0] == '0')
	{
		d = "";
		len = (data->let == 'p') ? 2 : 0;
		if (data->let == 'x' || data->let == 'X')
		{
			s = "";
			l = 0;
		}
	}
	if (data->prec == -1 && d[0] == '0' && l > 0 && data->let != 'p')
	{
		s = "";
		l = 0;
	}
	ft_putstr(s);
	sign_printing(data->flags, data->min);
	ft_putstr(d);
	return (len + sign_check(data->flags, data->min) + l);
}

int		len_less_prec_plus(t_p_list *data, char *s, int len, char *d)
{
	int l;

	l = (data->let == 'p') ? 0 : ft_strlen(s);
	if ((c_find(data->flags, '#') == 1) && (data->let == 'o' ||
		data->let == 'O'))
	{
		s = "";
		l = 0;
	}
	if (data->let == 'p')
		data->prec += 2;
	ft_putstr(s);
	sign_printing(data->flags, data->min);
	ft_put_len('0', (data->prec - len));
	ft_putstr(d);
	return (data->prec + sign_check(data->flags, data->min) + l);
}

int		len_prec_plus(t_p_list *d, char *s, int len, char *n)
{
	int l;

	l = (d->let == 'p') ? 0 : ft_strlen(s);
	if ((c_find(d->flags, '0') == 1) && d->prec == -1)
	{
		ft_putstr(s);
		sign_printing(d->flags, d->min);
		ft_put_len('0', (d->width - l - (len + sign_check(d->flags, d->min))));
		ft_putstr(n);
		return (d->width);
	}
	else
	{
		if (d->prec == 0 && n[0] == '0')
		{
			n = "";
			len = 0;
		}
		ft_put_len(' ', (d->width - l - (len + sign_check(d->flags, d->min))));
		sign_printing(d->flags, d->min);
		ft_putstr(s);
		ft_putstr(n);
		return (d->width);
	}
}

int		len_less_plus(t_p_list *data, char *s, int len, char *d)
{
	int ret;
	int l;

	ret = 0;
	l = (data->let == 'p') ? 0 : ft_strlen(s);
	if (data->width >= data->prec)
	{
		if (data->let == 'p')
			data->prec += 2;
		ret = ft_put_len(' ', (data->width - l -
			(data->prec + sign_check(data->flags, data->min))));
		ret += sign_printing(data->flags, data->min);
		ft_putstr(s);
		ret += ft_put_len('0', (data->prec - len));
		ft_putstr(d);
		return (data->width);
	}
	else
	{
		ft_putstr(s);
		ret += sign_printing(data->flags, data->min);
		ret += ft_put_len('0', (data->prec - len));
		ft_putstr(d);
		return (len + ret + l);
	}
}

int		padding_d(t_p_list *data, int len, char *d)
{
	intmax_t	ret;
	char		*s;
	int			l;

	s = hash_checking(data->flags, data->let);
	l = (data->let == 'p') ? 0 : ft_strlen(s);
	if (len >= data->width)
	{
		if (len >= data->prec)
			return (len_higher_prec_plus(data, d, s, len));
		else
			return (ret = len_less_prec_plus(data, s, len, d));
	}
	else
	{
		if (len >= data->prec)
			return (len_prec_plus(data, s, len, d));
		else
			return (len_less_plus(data, s, len, d));
	}
	return (0);
}
