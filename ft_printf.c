/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvynokur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 19:09:19 by dvynokur          #+#    #+#             */
/*   Updated: 2017/02/12 19:09:26 by dvynokur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			(*g_arg_manipulating[10])(t_p_list *data, va_list *ap) =
{func_s, func_p, func_d, func_o, func_u, func_x, func_c, func_b, func_s_big};

t_p_list	*struct_making(void)
{
	t_p_list *data;

	if (!(data = (t_p_list *)malloc(sizeof(t_p_list))))
		return (NULL);
	data->flags = ft_strnew(5);
	data->width = 0;
	data->prec = -1;
	data->size_mod = ft_strnew(2);
	data->let = '\0';
	data->i_after = 0;
	data->min = 0;
	data->next = NULL;
	return (data);
}

int			k_count(char *str, int i)
{
	i++;
	while (str[i])
	{
		if (str[i + 1] == '\0')
			return (i);
		if (str[i] == '-' || str[i] == '+' || str[i] == '0' || str[i] == ' '
		|| str[i] == '#' || (str[i] >= '0' && str[i] <= '9') || str[i] == '.'
		|| str[i] == '*' || str[i] == 'L' || str[i] == 'l' || str[i] == 'h'
		|| str[i] == 'j' || str[i] == 'z')
			i++;
		else
			return (i);
	}
	return (0);
}

void		f_flag(t_p_list *data, char *str)
{
	int		a;
	int		i;

	i = 1;
	a = 0;
	if (c_find(str, '-') == 1)
		data->flags[a++] = '-';
	if (c_find(str, '+') == 1)
		data->flags[a++] = '+';
	if (c_find(str, ' ') == 1)
		data->flags[a++] = ' ';
	if (c_find(str, '#') == 1)
		data->flags[a++] = '#';
	while (str[i])
	{
		if ((str[0] == '0') || (str[i] == '0' && !(str[i - 1] >= '0' &&
			str[i - 1] <= '9')))
			data->flags[a] = '0';
		i++;
	}
	f_size(data, str);
}

t_p_list	*str_analysing(char *str, va_list *ap)
{
	t_p_list	*data;
	t_p_list	*temp;
	char		*s;
	int			i;
	int			k;

	s = NULL;
	data = struct_making();
	temp = data;
	i = -1;
	k = 0;
	while (str[++i])
		if ((str[i] == '%') && (str[i + 1] != '\0'))
		{
			k = k_count(str, i);
			s = ft_strsub(str, i + 1, (k - i - 1));
			f_width(data, s, ap);
			free(s);
			data->let = str[k];
			data->i_after = k + 1;
			data->next = struct_making();
			data = data->next;
			i = k;
		}
	return (temp);
}

int			ft_printf(const char *f, ...)
{
	va_list		ap;
	t_p_list	*str_info;
	int			i;
	int			count;

	i = 0;
	count = 0;
	va_start(ap, f);
	str_info = str_analysing((char *)f, &ap);
	while ((f[i] != '%' && f[i] != '\0') || (f[i] == '%' && f[i + 1] != '\0'))
	{
		if ((f[i] == '%') && (f[i + 1] != '\0'))
		{
			if (str_info->let == 'n')
				*(va_arg(ap, uintmax_t *)) = count;
			count += g_arg_manipulating[f_type(str_info)](str_info, &ap);
			i = str_info->i_after;
			str_info = str_info->next;
		}
		else if (++count)
			ft_putchar(f[i++]);
	}
	free(str_info);
	va_end(ap);
	return (count);
}
