/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvynokur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/15 19:24:08 by dvynokur          #+#    #+#             */
/*   Updated: 2017/02/15 19:25:41 by dvynokur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <stdarg.h>
# include <stdio.h>
# include <stdlib.h>
# include "libft/libft.h"
# include <stdint.h>

typedef struct			s_print_list
{
	char				*flags;
	int					width;
	int					prec;
	char				*size_mod;
	char				let;
	int					i_after;
	int					min;
	struct s_print_list	*next;
}						t_p_list;
void					f_flag(t_p_list *data, char *str);
void					f_width(t_p_list *data, char *str, va_list *ap);
void					f_precision(t_p_list *data, char *str, va_list *ap);
void					f_size(t_p_list *data, char *str);
int						f_type(t_p_list *data);
int						func_s(t_p_list *data, va_list *ap);
int						func_p(t_p_list *data, va_list *ap);
int						func_d(t_p_list *data, va_list *ap);
int						func_o(t_p_list *data, va_list *ap);
int						func_u(t_p_list *data, va_list *ap);
int						func_x(t_p_list *data, va_list *ap);
int						func_c(t_p_list *data, va_list *ap);
int						func_c_big(t_p_list *data, va_list *ap);
int						func_b(t_p_list *data, va_list *ap);
int						func_s_big(t_p_list *data, va_list *ap);
int						func_percent(t_p_list *data);
int						ft_printf(const char *format, ...);
int						c_find(char *s, char c);
size_t					len_calc(uintmax_t d, int n);
int						ft_putstr_len(char *str, int precision);
int						ft_put_len(char c, int width);
int						padding_s(int width, int precision, int len, char a);
int						sign_printing(char *flags, int d);
int						sign_check(char *flags, int d);
int						k_count(char *str, int i);
void					f_width_star(t_p_list **data, char *str, va_list *ap);
void					f_width_star_dot(t_p_list **data, char *s1,
						char *s2, va_list *ap);
char					*ft_itoa_max(intmax_t n);
uintmax_t				size_mod_u(t_p_list *data, va_list *ap);
int						padding_d_min(t_p_list *data,
						int len, char *d);
int						padding_d(t_p_list *data, int len, char *d);
intmax_t				size_mod(t_p_list *data, va_list *ap);
char					*ft_itoa_base_u(uintmax_t n, int base, int up);
int						padding_char(t_p_list *data, char a);
uintmax_t				size_mod_x(t_p_list *data, va_list *ap);
int						shifting_bytes(unsigned int c);
int						padding_wchar(t_p_list *data, unsigned int c, int n);
int						counting_s_big(wchar_t *s);
int						ft_put_s_big(wchar_t *s, int precision);
int						char_len(unsigned int c);
char					*hash_checking(char *flags, char let);
int						len_higher_prec_min(t_p_list *data,
						char *d, int len, char *s);
int						len_prec_min(t_p_list *data,
						char *s, char *d, int l);
int						len_less_prec(t_p_list *data, char *s,
						char *d, int l);
int						len_higher_prec_plus(t_p_list *data,
						char *d, char *s, int l);
int						len_less_prec_plus(t_p_list *data,
						char *s, int len, char *d);
int						len_prec_plus(t_p_list *data, char *s,
						int len, char *d);
int						len_less_plus(t_p_list *data,
						char *s, int len, char *d);
int						char_w_higher(t_p_list *data, char a, char c);
int						prec_higher_one(wchar_t *s, int precision);
int						shifting_bytes_big(unsigned int c);
int						width_higher_one(t_p_list *data,
						unsigned int a, int n, char c);
int						s_big_len_higher(int precision, int width,
						int len, char a);
void					f_width_h(t_p_list *data, char *str, va_list *ap);
int						(*g_arg_manipulating[10])(t_p_list *data, va_list *ap);
uintmax_t				size_mod_u_other(t_p_list *data, va_list *ap);

#endif
