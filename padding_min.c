/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   padding_min.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvynokur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/08 18:04:37 by dvynokur          #+#    #+#             */
/*   Updated: 2017/04/08 18:04:38 by dvynokur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*hash_checking(char *flags, char conv_letter)
{
	char *s;

	s = ft_strnew(2);
	if ((c_find(flags, '#') == 1) || conv_letter == 'p')
	{
		if (conv_letter == 'x' || conv_letter == 'p')
			s = "0x";
		if (conv_letter == 'X')
			s = "0X";
		if (conv_letter == 'o' || conv_letter == 'O')
			s = "0";
	}
	return (s);
}

int		len_higher_prec_min(t_p_list *data, char *d, int len, char *s)
{
	int l;

	l = 0;
	if (data->let != 'p')
		l = ft_strlen(s);
	if (data->prec == 0 && d[0] == '0')
	{
		d = "";
		len = 0;
		if (data->let == 'p')
			len = 2;
		if (data->let == 'x' || data->let == 'X')
		{
			s = "";
			l = 0;
		}
	}
	if (data->prec == -1 && d[0] == '0' && l > 0 && data->let != 'p')
	{
		s = "";
		l = 0;
	}
	ft_putstr(s);
	ft_putstr(d);
	return (len + l);
}

int		len_prec_min(t_p_list *data, char *s, char *d, int l)
{
	int len;

	len = ft_strlen(d);
	if (len >= data->prec)
	{
		if (data->prec == 0 && d[0] == '0')
		{
			d = "";
			len = 0;
		}
		ft_putstr(s);
		ft_putstr(d);
		ft_put_len(' ', l);
		return (data->width);
	}
	else
	{
		ft_putstr(s);
		if (data->let == 'p')
			data->prec += 2;
		ft_put_len('0', (data->prec - len));
		ft_putstr(d);
		return (data->prec + l);
	}
	return (0);
}

int		len_less_prec(t_p_list *data, char *s, char *d, int l)
{
	int ret;
	int len;

	ret = 0;
	len = ft_strlen(d);
	if (data->width >= data->prec)
	{
		if (data->let == 'p')
			data->prec += 2;
		ft_putstr(s);
		ret += ft_put_len('0', (data->prec - len));
		ft_putstr(d);
		ret += ft_put_len(' ', (data->width - l - data->prec -
			sign_check(data->flags, data->min)));
		return (data->width + l);
	}
	else
	{
		ft_putstr(s);
		ret += ft_put_len('0', (data->prec - len));
		ft_putstr(d);
		return (data->prec + l);
	}
}

int		padding_d_min(t_p_list *data, int len, char *d)
{
	char		*s;
	int			l;

	s = hash_checking(data->flags, data->let);
	l = (data->let != 'p') ? ft_strlen(s) : 0;
	sign_printing(data->flags, data->min);
	if (len >= data->width)
	{
		if (len >= data->prec)
			return (len_higher_prec_min(data, d, len, s));
		else
			return (len_prec_min(data, s, d, l));
	}
	else
	{
		if (len >= data->prec)
			return (len_prec_min(data, s, d,
				(data->width - l - (len +
					sign_check(data->flags, data->min)))));
		else
			return (len_less_prec(data, s, d, l));
	}
	return (0);
}
