/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct_making.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvynokur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 21:40:49 by dvynokur          #+#    #+#             */
/*   Updated: 2017/02/17 21:40:53 by dvynokur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	f_width(t_p_list *data, char *str, va_list *ap)
{
	if (str[0] >= '0' && str[0] <= '9')
		data->width = ft_atoi(&str[0]);
	if (str[0] == '*')
		data->width = va_arg(*ap, int);
	if (data->width < 0)
	{
		data->width *= -1;
		data->flags = ft_strjoin(data->flags, "-");
	}
	f_width_h(data, str, ap);
	f_flag(data, str);
	f_precision(data, str, ap);
}

void	f_width_h(t_p_list *data, char *str, va_list *ap)
{
	int i;

	i = 1;
	while (str[i])
	{
		if ((str[i] >= '0' && str[i] <= '9') && (str[i - 1] != '.') &&
			!(str[i - 1] >= '0' && str[i - 1] <= '9'))
		{
			data->width = ft_atoi(&str[i]);
			while (str[i] >= '0' && str[i] <= '9')
				i++;
			i--;
		}
		else if (str[i] == '*' && str[i - 1] != '.')
		{
			data->width = va_arg(*ap, int);
			if (data->width < 0)
			{
				data->width *= -1;
				data->flags = ft_strjoin(data->flags, "-");
			}
		}
		i++;
	}
}

void	f_precision(t_p_list *data, char *str, va_list *ap)
{
	int		i;

	i = -1;
	while (str[++i])
	{
		if (str[i] == '.')
			data->prec = 0;
		if (str[i] == '.' && str[i + 1] >= '1' && str[i + 1] <= '9')
			data->prec = ft_atoi(&str[i + 1]);
		if (str[i] == '.' && str[i + 1] == '0')
			data->prec = 0;
		if (str[i] == '.' && str[i + 1] == '*')
		{
			data->prec = va_arg(*ap, int);
			if (data->prec < 0)
				data->prec = -1;
		}
	}
}

int		f_size_h(char *str)
{
	int i;
	int last;

	i = 1;
	last = (ft_strlen(str) - 1);
	if (str[0] == 'h' && str[1] != 'h')
		return (1);
	if (str[last] == 'h' && str[last - 1] != 'h')
		return (1);
	if (last < 2)
		return (0);
	while (i != (last - 1))
	{
		if (str[i] == 'h' && str[i + 1] != 'h' && str[i - 1] != 'h')
			return (1);
		i++;
	}
	return (0);
}

void	f_size(t_p_list *p, char *str)
{
	int	i;

	i = -1;
	while (str[++i])
	{
		if ((c_find(str, 'j') == 1) && (p->size_mod[0] = 'j'))
			break ;
		if ((c_find(str, 'z') == 1) && (p->size_mod[0] = 'z'))
			break ;
		if ((ft_strstr(str, "ll") != NULL) && (p->size_mod[0] = 'l'))
		{
			p->size_mod[1] = 'l';
			break ;
		}
		if ((c_find(str, 'l') == 1) && (p->size_mod[0] = 'l'))
			break ;
		if ((f_size_h(str) == 1) && (p->size_mod[0] = 'h'))
			break ;
		if ((ft_strstr(str, "hh") != NULL) && (p->size_mod[0] = 'h'))
		{
			p->size_mod[1] = 'h';
			break ;
		}
	}
}
