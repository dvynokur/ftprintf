/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   S_func.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dvynokur <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 12:54:06 by dvynokur          #+#    #+#             */
/*   Updated: 2017/04/09 12:54:09 by dvynokur         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		counting_s_big(wchar_t *s)
{
	int i;
	int	count;
	int num;

	i = 0;
	count = 0;
	num = 0;
	while (s[i] != '\0')
	{
		num = char_len(s[i]);
		if (num == 1)
			count++;
		else if (num == 2)
			count += 2;
		else if (num == 3)
			count += 3;
		else
			count += 4;
		i++;
	}
	return (count);
}

int		prec_higher_one(wchar_t *s, int precision)
{
	char	*num;
	int		i;
	int		count;

	i = -1;
	count = 0;
	while (s[++i] && (precision > 0))
	{
		num = ft_itoa_base_u(s[i], 2, 0);
		if (ft_strlen(num) <= 7)
		{
			ft_putchar((char)s[i]);
			precision--;
			count++;
		}
		else
		{
			if ((precision -= char_len(s[i])) > 0)
				count += shifting_bytes(s[i]);
			else
				return (count);
		}
	}
	return (count);
}

int		ft_put_s_big(wchar_t *s, int precision)
{
	int i;
	int count;

	i = -1;
	count = 0;
	if (precision == -1)
	{
		while (s[++i])
			shifting_bytes(s[i]);
		count = counting_s_big(s);
	}
	else
		count = prec_higher_one(s, precision);
	return (count);
}

int		char_len(unsigned int c)
{
	char	*num;

	num = ft_itoa_base_u(c, 2, 0);
	if (ft_strlen(num) <= 7)
		return (1);
	else if (ft_strlen(num) <= 11)
		return (2);
	else if (ft_strlen(num) <= 16)
		return (3);
	else
		return (4);
	return (0);
}

int		func_s_big(t_p_list *data, va_list *ap)
{
	wchar_t	*s;
	int		len;
	char	a;
	int		ret;

	a = ' ';
	ret = 0;
	len = 0;
	s = va_arg(*ap, wchar_t *);
	if (s == NULL)
		s = L"(null)";
	len = counting_s_big(s);
	if (c_find(data->flags, '0') == 1)
		a = '0';
	if (c_find(data->flags, '-') == 1)
	{
		ret = ft_put_s_big(s, data->prec);
		ret += padding_s(data->width, data->prec, len, a);
	}
	else
	{
		ret = padding_s(data->width, data->prec, len, a);
		ret += ft_put_s_big(s, data->prec);
	}
	return (ret);
}
